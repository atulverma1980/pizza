import React,{Component} from "react";
import "bootstrap/dist/css/bootstrap.min.css";
class ShowCartA8B extends Component{
    render(){
        const{data,index,val,onPlus,onMinus}=this.props;
        return(<React.Fragment>
            <div className="row border">
                <div className="col-4">
                    <img className="img-fluid" src={data.image}/>
                </div>
                <div className="col-5">
                    <div className="text-center">
                        <h6>{data.name}</h6>
                    </div>
                    <div className="text-justify">
                        {data.desc}
                    </div>
                  
                       <div className="text-center">
                          <h6>{(data.size!=undefined && data.crust!=undefined)?data.size+" ||"+data.crust:""}</h6>
                           </div>
               
                   <div className="text-center">
                   <button className="btn btn-success m-1" onClick={()=>onPlus(data.id,index)}>+</button>
                   <button className="btn btn-secondary disabled">{data.qty}</button>
                   <button className="btn btn-danger m-1" onClick={()=>onMinus(data.id,index)}>-</button>
                   </div>
                </div>
            </div>
        </React.Fragment>);
    }
}
export default ShowCartA8B;