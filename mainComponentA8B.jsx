import React,{Component} from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import ShowPizzaA8 from "./showPizzaA8";
import ShowCartA8B from "./showCartA8B";
class MainComponentA8B extends Component{
    state={
        data:[
            {"id":"MIR101","image":"https://i.ibb.co/SR1Jzpv/mirinda.png","type":"Beverage","name":"Mirinda","desc":"Mirinda","veg":"Yes"},
            {"id":"PEP001","image":"https://i.ibb.co/3vkKqsF/pepsiblack.png","type":"Beverage","name":"Pepsi Black	Can","desc":"Pepsi	Black	Can","veg":"Yes"},
            {"id":"LIT281","image":"https://i.ibb.co/27PvTng/lit.png","type":"Beverage","name":"Lipton	Iced Tea","desc":"Lipton	Iced	Tea","veg":"Yes"},
            {"id":"PEP022","image":"https://i.ibb.co/1M9xDZB/pepsi-new20190312.png","type":"Beverage","name":"Pepsi	New","desc":"Pepsi	New","veg":"Yes"},
            {"id":"BPCNV1","image":"https://i.ibb.co/R0VSJjq/Burger-Pizza-Non-Veg-nvg.png","type":"Burger Pizza","name":"Classic	Non	Veg","desc":"Oven-baked	buns	with	cheese,	peri-peri	chicken,	tomato	&	capsicum	in	creamy	mayo","veg":"No"},
            {"id":"BPCV03","image":"https://i.ibb.co/Xtx43fT/Burger-Pizza-Veg-423-X420-Pixel1.png","type":"Burger Pizza","name":"Classic	Veg","desc":"Oven-baked	buns	with	cheese,	tomato	&	capsicum	in	creamy	mayo","veg":"Yes"},
            {"id":"BPPV04","image":"https://i.ibb.co/Xtx43fT/Burger-Pizza-Veg-423-X420-Pixel1.png","type":"Burger Pizza","name":"Premium	Veg","desc":"Oven-baked	buns	with	cheese,	paneer,	tomato,	capsicum	&	red	paprika	in	creamy	mayo","veg":"Yes"},
            {"id":"DIP033","image":"https://i.ibb.co/0mbBzsw/new-cheesy.png","type":"Side	Dish","name":"Cheesy	Dip","desc":"An	all-time	favorite	with	your	Garlic	Breadsticks	&	Stuffed	Garlic	Bread	for	a	Cheesy	indulgence","veg":"Yes"},
            {"id":"DIP072","image":"https://i.ibb.co/fY52zBw/new-jalapeno.png","type":"Side	Dish","name":"Cheesy	Jalapeno	Dip","desc":"A	spicy,	tangy	flavored	cheese	dip	is	a	an	absolute	delight	with	your	favourite	Garlic	Breadsticks","veg":"Yes"},
            {"id":"GAR952","image":"https://i.ibb.co/BNVmfY9/Garlic-bread.png","type":"Side	Dish","name":"Garlic	Breadsticks","desc":"Baked	to	perfection.	Your	perfect	pizza	partner!	Tastes	best	with	dip","veg":"Yes"},
            {"id":"PARCH1","image":"https://i.ibb.co/prBs3NJ/Parcel-Nonveg.png","type":"Side	Dish","name":"Chicken	Parcel","desc":"Snacky	bites!	Pizza	rolls	with	chicken	sausage	&	creamy	harissa	sauce","veg":"No"},
            {"id":"PARVG7","image":"https://i.ibb.co/JHhrM7d/Parcel-Veg.png","type":"Side	Dish","name":"Veg	Parcel","desc":"Snacky	bites!	Pizza	rolls	with	paneer	&	creamy	harissa	sauce","veg":"Yes"},
            {"id":"PATNV7","image":"https://i.ibb.co/0m89Jw9/White-Pasta-Nvg.png","type":"Side	Dish","name":"White	Pasta	Italiano	Non-Veg","desc":"Creamy	white	pasta	with	pepper	barbecue	chicken","veg":"No"},
            {"id":"PATVG4","image":"https://i.ibb.co/mv8RFbk/White-Pasta-Veg.png","type":"Side	Dish","name":"White	Pasta	Italiano	Veg","desc":"Creamy	white	pasta	with	herb	grilled	mushrooms","veg":"Yes"},
            {"id":"DES044","image":"https://i.ibb.co/gvpDKPv/Butterscotch.png","type":"Dessert","name":"Butterscotch	Mousse	Cake","desc":"Sweet	temptation!	Butterscotch	flavored	mousse","veg":"Yes"},
            {"id":"DES028","image":"https://i.ibb.co/nm96NZW/ChocoLava.png","type":"Dessert","name":"Choco	Lava	Cake","desc":"Chocolate	lovers	delight!	Indulgent,	gooey	molten	lava	inside	chocolate	cake","veg":"Yes"},
            {"id":"PIZVDV","image":"https://i.ibb.co/F0H0SWG/deluxeveg.png","type":"Pizza","name":"Deluxe	Veggie","desc":"Veg	delight	- onion,	capsicum,	grilled	mushroom,	corn	&	paneer","veg":"Yes"},
            {"id":"PIZVFH","image":"https://i.ibb.co/4mHxB5x/farmhouse.png","type":"Pizza","name":"Farmhouse","desc":"Delightful	combination	of	onion,	capsicum,	tomato	&	grilled	mushroom","veg":"Yes"},
            {"id":"PIZVIT","image":"https://i.ibb.co/sRH7Qzf/Indian-TandooriPaneer.png","type":"Pizza","name":"Indi	Tandoori	Paneer","desc":"It	is	hot.	It	is	spicy.	It	is	oh-soIndian.	Tandoori	paneer	with	capsicum,	red	paprika	&	mint	mayo","veg":"Yes"},
            {"id":"PIZVMG","image":"https://i.ibb.co/MGcHnDZ/mexgreen.png","type":"Pizza","name":"Mexican	Green	Wave","desc":"Mexican	herbs	sprinkled	on	onion,	capsicum,	tomato	&	jalapeno","veg":"Yes"},
            {"id":"PIZVPP","image":"https://i.ibb.co/cb5vLX9/peppypaneer.png","type":"Pizza","name":"Peppy	Paneer","desc":"Flavorful	trio	of	juicy	paneer,	crisp	capsicum	with	spicy	red	paprika","veg":"Yes"},
            {"id":"PIZVVE","image":"https://i.ibb.co/gTy5DTK/vegextra.png","type":"Pizza","name":"Veg	Extravaganza","desc":"Black	olives,	capsicum,	onion,	grilled	mushroom,	corn,	tomato,	jalapeno	&	extra	cheese","veg":"Yes"},
            {"id":"PIZNCP","image":"https://i.ibb.co/b5qBJ9d/cheesepepperoni.png","type":"Pizza","name":"Chicken	Pepperoni","desc":"A	classic	American	taste!	Relish	the	delectable	flavor	of	Chicken	Pepperoni,	topped	with	extra	cheese","veg":"No"},
            {"id":"PIZNCD","image":"https://i.ibb.co/GFtkbB1/ChickenDominator10.png","type":"Pizza","name":"Chicken	Dominator","desc":"Loaded	with	double	pepper	barbecue	chicken,	peri-peri	chicken,	chicken	tikka	&	grilled	chicken	rashers","veg":"No"},
            {"id":"PIZNPB","image":"https://i.ibb.co/GxbtcLK/Pepper-Barbeque-OnionC.png","type":"Pizza","name":"Pepper	Barbecue	&	Onion","desc":"A	classic	favourite	with	pepper	barbeque	chicken	&	onion","veg":"No"},
            {"id":"PIZNIC","image":"https://i.ibb.co/6Z5wBqr/Indian-Tandoori-ChickenTikka.png","type":"Pizza","name":"Indi	Chicken	Tikka","desc":"The	wholesome	flavour	of	tandoori	masala	with	Chicken	tikka,	onion,	red	paprika	&	mint	mayo","veg":"No"}
            ],
            
 sizes	:	["Regular","Medium","Large"],
 crusts	:	["New Hand Tossed","Wheat Thin Crust","Cheese Burst","Fresh Pan Pizza","Classic Hand Tossed"],
            view:-1,
            arr:[],
            cart:[],
            val:-1,
            ind:-1,
            pr:{
                size:"",
                crust:""
            },
          
    };
    handlePlus=(id,index)=>{
        let s1 = {...this.state};
        let fnd=s1.data.find(v=>v.id==id);
        let fnd1 = s1.cart.find(v=>v.id==id);
        fnd.qty++;
        this.setState(s1);
    }
    handleMinus=(id,index)=>{
        let s1 = {...this.state};
        let fnd=s1.data.find(v=>v.id==id);
        let fnd1 = s1.cart.find(v=>v.id==id);
        fnd.qty--;
        if(fnd.qty<1){
            s1.cart.splice(index,1);
            fnd.val=-1;
            fnd.size="";
            fnd.crust="";
        }
      
        this.setState(s1);
    }
    showVeg=()=>{
        let s1 = {...this.state};
        s1.arr = s1.data.filter(v=>v.type=='Pizza' && v.veg=='Yes');
        s1.val=-1;
        s1.view=1;
        s1.pr.size="";
        s1.pr.crust="";
        this.setState(s1);
    }
    showNonVeg=()=>{
        let s1 = {...this.state};
        s1.arr = s1.data.filter(v=>v.type=='Pizza' && v.veg=='No');
        s1.val=-1;
        s1.view=1;
        s1.pr.size="";
        s1.pr.crust="";
        this.setState(s1);
    }
    showSide=()=>{
        let s1 = {...this.state};
        s1.arr = s1.data.filter(v=>v.type=='Side	Dish');
        s1.view=1;
        s1.val=1;
        this.setState(s1);
    }
    showOther=()=>{
        let s1 = {...this.state};
        s1.arr = s1.data.filter(v=>v.type=='Burger Pizza' || v.type=='Beverage' ||v.type=='Dessert');
        s1.view=1;
        s1.val=1;
        this.setState(s1);
    }
    handleAdd=(index,prod)=>{
        let s1 = {...this.state};
        console.log(prod);
        if(s1.val==-1){
        if(prod.size==""){
            alert("Please Select Size");
           
        }else if(prod.crust==""){
            alert("Please Select Crust"); 
        }else{
            s1.arr[index].val=index;
            s1.arr[index].qty=1;
            s1.arr[index].size=prod.size;
            s1.arr[index].crust=prod.crust;
            s1.cart.push(s1.arr[index]);
           s1.pr.size="";
           s1.pr.crust="";
        }
    }else{
        s1.arr[index].val=index;
        s1.arr[index].qty=1;
        s1.cart.push(s1.arr[index]);
    }
    
    this.setState(s1);
    }
    render(){
        const{data,view,arr,val,sizes,crusts,cart,pr}=this.state;

        return(<React.Fragment>
             <div className="container-fluid">
            <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
                <a className="navbar-brand text-white" href="#" >
                    MyFavPizza
                </a>
              <div className="" id="navbarSupportedContent">
                  <ul className="navbar-nav ">
                      <li className="nav-item ">
                          <a className="nav-link" href="#">
                              <span onClick={()=>this.showVeg()}>
                                Veg Pizza
                              </span>
                          </a>
                      </li>
                      <li className="nav-item ">
                          <a className="nav-link" href="#">
                              <span onClick={()=>this.showNonVeg()}>
                                  Non-veg Pizza
                              </span>
                          </a>
                      </li>
                      <li className="nav-item ">
                          <a className="nav-link" href="#">
                              <span onClick={()=>this.showSide()} >
                                  Side Dishes
                              </span>
                          </a>
                      </li>
                      <li className="nav-item ">
                          <a className="nav-link" href="#">
                              <span onClick={()=>this.showOther()} >
                                Other Items
                              </span>
                          </a>
                      </li>
                  </ul>
              </div>  
            </nav> 
            {view==1?(
            <div className="row my-5">
                <div className="col-7 border">
                    <div className="row">
                   {arr.map((v,index)=><ShowPizzaA8  data={v}index={index} pr={pr} sizes={sizes} crusts={crusts} val={val} onPlus={this.handlePlus} onMinus={this.handleMinus} onAdd={this.handleAdd}/>)} 
                    
                  </div> 
                </div>
                <div className="col-5 border">
                    {cart.length==0?<h5 className="text-center">Cart is Empty</h5>:<h5 className="text-center">My Cart</h5>}
                    {cart.map((v,index)=><ShowCartA8B cart={cart} data={v} index={index} onPlus={this.handlePlus} onMinus={this.handleMinus} val={val}/>)}
                </div>
            </div>):""}

            </div>


        </React.Fragment>)
    }
}
export default MainComponentA8B;