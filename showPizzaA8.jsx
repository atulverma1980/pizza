import React,{Component} from "react";
import "bootstrap/dist/css/bootstrap.min.css";
class ShowPizzaA8 extends Component{
    state={
      pr:this.props.pr,
    };
    handleChange=(e)=>{
        const{currentTarget:input}=e;
        let s1 = {...this.state};
        s1.pr[input.name]=input.value;
        this.setState(s1);
    }
   
    render(){
        const{data,index,sizes,crusts,val,onAdd,onPlus,onMinus}=this.props;
        const{size,crust}=this.state.pr;
        return (<React.Fragment>
            <div className="col-6 border">
                <div className="row">
                <img className="img-fluid" src={data.image}/>
                <div className="col-4"></div>
                <h6 className="text-center">{data.name}</h6><br/>
               <br/> <div className="text-justify"><p>{data.desc}</p></div>
                {val==-1?(
                <div className="row">
                    <div className="col-1"></div>
                <div className="col-5">
                    {data.val==index?this.showDD("Select Size",sizes,"size",data.size,"disabled"):(this.showDD1("Select Size",sizes,"size",size))}
                </div>
                <div className="col-5">
                {data.val==index?this.showDD("Select Crusts",crusts,"crust",data.crust,"disabled"):this.showDD1("Select Crusts",crusts,"crust",crust)}
                </div>
                </div>):""}
            </div>
            <div className="text-center">
                {index==data.val?(<React.Fragment>
                    <button className="btn btn-success m-1" onClick={()=>onPlus(data.id,index)}>+</button>
                    <button className="btn btn-secondary disabled">{data.qty}</button>
                    <button className="btn btn-danger m-1"onClick={()=>onMinus(data.id,index)}>-</button>
                </React.Fragment>):(
                <button className="btn btn-primary m-2" onClick={()=>onAdd(index,this.state.pr)}>Add to Cart</button>)}
            </div>
            </div>
        </React.Fragment>);
    }
    showDD=(header,arr,name,value,dd)=>{
        console.log(value);
        return(<React.Fragment>
            <select className="form-control" name={name} disabled={dd}  value={value} onChange={this.handleChange}>
                <option value="" selected disabled>{header}</option>
                {arr.map(v=><option>{v}</option> )}
            </select>
        </React.Fragment>)
    }
    showDD1=(header,arr,name,value)=>{
        console.log(value);

        return(<React.Fragment>
            <select className="form-control" name={name}   value={value} onChange={this.handleChange}>
                <option value="" selected>{header}</option>
                {arr.map(v=><option>{v}</option> )}
            </select>
        </React.Fragment>)
    }
   
}
export default ShowPizzaA8;